#pragma once

#include <pugixml.hpp>
#include <string>
#include <vector>


using namespace pugi;

/**
* A parameter to be used in SynDomUtils::extract_from_node.
*/
struct ExtractionParam {
    /**
    * Describes where the target value has to be searched
    */
    enum ParamType {
        CHILD, /*!< The target value is inside the content of a node */
        ATTRIBUTE /*!< The target value is the attribute value of a node */
    };

    ParamType type{CHILD};

    /**
    * The node hierarchy to be traversed. Can be an empty std::vector, in which
    * case it means that there's no need to traverse any other node other than
    * the root node.
    */
    std::vector<std::string> tags{};

    /**
    * In case ExtractionParam::type is `ATTRIBUTE`, the name of the
    * attribute.
    */
    std::string attribute{""};
};
