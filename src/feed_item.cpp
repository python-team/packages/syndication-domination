#include "feed_item.hpp"
#include "utils.hpp"

std::string FeedItem::extract_url() {
    std::string res = item_node.child("link").text().as_string();
    if (!res.empty()) return res;
    res = SynDomUtils::extract_link(
        item_node, {"alternate"}, {"text/html"}, false, true
    );
    if (!res.empty()) return res;
    res = item_node.child("id").text().as_string();
    if (SynDomUtils::is_url(res)) return res;
    return "";
}

std::string FeedItem::extract_media_url() {
    std::string res = "";
    xml_node enclosure = item_node.child("enclosure");
    std::string type = enclosure.attribute("type").value();
    if (
            SynDomUtils::str_has_prefix(type, "audio/") ||
            SynDomUtils::str_has_prefix(type, "video/")
    ) {
        res = enclosure.attribute("url").value();
    }
    return res;
}

std::string FeedItem::extract_img_url() {
    std::string res;

    // this part is sufficiently unique to this case, can be ad-hoc
    xml_node enclosure = item_node.child("enclosure");
    while (enclosure) {
        if (SynDomUtils::str_has_prefix(
                enclosure.attribute("type").value(), "image/")
        ) {
            res = enclosure.attribute("url").value();
            if (!res.empty()) return res;
        }
        enclosure = enclosure.next_sibling("enclosure");
    }

    res = SynDomUtils::extract_from_node(item_node, __IMG_URL_PARAMS);
    if (!res.empty()) return res;

    return "";
}

void FeedItem::fix_url(std::string &s) {
    SynDomUtils::trim(s);
    if (s.empty() || SynDomUtils::is_url(s)) return;
    if (SynDomUtils::str_has_prefix(s, "/") && s != website_url) {
        s = website_url + s;
        return;
    }
    s = "http://" + s;
}

void FeedItem::parse() {
    // title, should be universally the same everywhere
    title = item_node.child("title").text().as_string();

    // content
    content = SynDomUtils::extract_from_node(item_node, __CONTENT_PARAMS);

    // url, the upstream link to the article
    url = extract_url();
    fix_url(url);

    // media url, the link to the audio or video for podcasts
    media_url = extract_media_url();
    fix_url(media_url);

    // pub_date
    pub_date = SynDomUtils::extract_from_node(item_node, __PUB_DATE_PARAMS);

    // img_url
    img_url = extract_img_url();
    fix_url(img_url);
}

std::string FeedItem::to_json() {
    return "        {\n"
        "            \"title\": \"" + title + "\",\n"
        "            \"content\": \"" + content + "\",\n"
        "            \"url\": \"" + url + "\",\n"
        "            \"media_url\": \"" + media_url + "\",\n"
        "            \"pub_date\": \"" + pub_date + "\",\n"
        "            \"img_url\": \"" + img_url + "\"\n"
        "        }";
}
