#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "feed.hpp"
#include "opml.hpp"
#ifdef HTML_SUPPORT
#include "html.hpp"
#endif


namespace py = pybind11;

PYBIND11_MODULE(syndom, m) {
    m.doc() = "Python bindings for SyndicationDomination";
    py::class_<FeedItem>(m, "FeedItem")
        .def_property_readonly("title", &FeedItem::get_title)
        .def_property_readonly("content", &FeedItem::get_content)
        .def_property_readonly("url", &FeedItem::get_url)
        .def_property_readonly("media_url", &FeedItem::get_media_url)
        .def_property_readonly("pub_date", &FeedItem::get_pub_date)
        .def_property_readonly("img_url", &FeedItem::get_img_url)
        .def("get_title", &FeedItem::get_title)
        .def("get_content", &FeedItem::get_content)
        .def("get_url", &FeedItem::get_url)
        .def("get_media_url", &FeedItem::get_media_url)
        .def("get_pub_date", &FeedItem::get_pub_date)
        .def("get_img_url", &FeedItem::get_img_url);
    py::class_<Feed>(m, "Feed")
        .def(py::init<std::string>())
        .def_property_readonly("title", &Feed::get_title)
        .def_property_readonly("description", &Feed::get_description)
        .def_property_readonly("url", &Feed::get_url)
        .def_property_readonly("last_update", &Feed::get_last_update)
        .def_property_readonly("img_url", &Feed::get_img_url)
        .def_property_readonly("rss_url", &Feed::get_rss_url)
        .def_property_readonly("items", &Feed::get_items)
        .def("get_title", &Feed::get_title)
        .def("get_description", &Feed::get_description)
        .def("get_url", &Feed::get_url)
        .def("get_last_update", &Feed::get_last_update)
        .def("get_img_url", &Feed::get_img_url)
        .def("get_rss_url", &Feed::get_rss_url)
        .def("get_items", &Feed::get_items);

    py::class_<OpmlItem>(m, "OpmlItem")
        .def_property_readonly("title", &OpmlItem::get_title)
        .def_property_readonly("description", &OpmlItem::get_description)
        .def_property_readonly("url", &OpmlItem::get_url)
        .def_property_readonly("feed_url", &OpmlItem::get_feed_url)
        .def_property_readonly("categories", &OpmlItem::get_categories)
        .def_property_readonly("type", &OpmlItem::get_type)
        .def_property_readonly("language", &OpmlItem::get_language)
        .def("get_title", &OpmlItem::get_title)
        .def("get_description", &OpmlItem::get_description)
        .def("get_url", &OpmlItem::get_url)
        .def("get_feed_url", &OpmlItem::get_feed_url)
        .def("get_categories", &OpmlItem::get_categories)
        .def("get_type", &OpmlItem::get_type)
        .def("get_language", &OpmlItem::get_language);
    py::class_<Opml>(m, "Opml")
        .def(py::init<std::string, bool>(),
            py::arg("path"), py::arg("essentials_only") = false
        )
        .def_property_readonly("items", &Opml::get_items)
        .def("get_items", &Opml::get_items);

#ifdef HTML_SUPPORT
    py::class_<Html>(m, "Html")
        .def(py::init<std::string>())
        .def_static("from_string", &Html::from_string, py::arg("html_string"))
        .def_property_readonly("title", &Html::get_title)
        .def_property_readonly("icon_url", &Html::get_icon_url)
        .def_property_readonly("img_url", &Html::get_img_url)
        .def_property_readonly("rss_url", &Html::get_rss_url)
        .def_property_readonly("description", &Html::get_description)
        .def_property_readonly("body", &Html::get_body)
        .def_property_readonly("article", &Html::get_article)
        .def("get_title", &Html::get_title)
        .def("get_icon_url", &Html::get_icon_url)
        .def("get_img_url", &Html::get_img_url)
        .def("get_rss_url", &Html::get_rss_url)
        .def("get_description", &Html::get_description)
        .def("get_body", &Html::get_body)
        .def("get_article", &Html::get_article);
#endif
}
