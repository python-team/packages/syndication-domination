#include <cstring>
#include <string>
#include <iostream>
#include "feed.hpp"
#include "opml.hpp"
#ifdef HTML_SUPPORT
#include "html.hpp"
#endif

void usage(char* prgname) {
    std::cerr << "Usage:\n" <<
        prgname << " -h, --help\n\t" <<
            "print this help message" << std::endl <<
        prgname << " -f, --feed [--no-items] [path_to_feed.xml]...\n" <<
            "\tparse the provided feed(s) and print the result as json\n" <<
            "\t--no-items\tdon't print the extracted feed items" <<
            std::endl <<
        prgname << " -o, --opml [path_to_opml.xml]...\n" <<
            "\tparse the provided opml file(s) and print the result as json" <<
        std::endl;
#ifdef HTML_SUPPORT
    std::cerr <<
        prgname << " -m --html [--metadata-only] [path_to_page.html]...\n" <<
            "\tparse the provided HTML file(s) and print the result as json" <<
            "\t--metadata-only\tjust print the page metadata, leaving out "
            "article and body" <<
        std::endl;
#endif
    exit(1);
}

int main(int argc, char** argv) {
    if (
            argc < 3 ||
            std::strcmp(argv[1], "--help") == 0 ||
            std::strcmp(argv[1], "-h") == 0
    ) {
        usage(argv[0]);
    }

    int begin = 2;
    if (
            std::strcmp(argv[1], "-f") == 0 ||
            std::strcmp(argv[1], "--feed") == 0
    ) {
        bool no_items = false;

        if (std::strcmp(argv[2], "--no-items") == 0) {
            if (argc < 4) usage(argv[0]);
            no_items = true;
            begin = 3;
        }

        for (int i=begin; i<argc; i++) {
            Feed f{argv[i]};
            std::cout << f.to_json(no_items);
        }
    }
    else if (
            std::strcmp(argv[1], "-o") == 0 ||
            std::strcmp(argv[1], "--opml") == 0
    ) {

        for (int i=begin; i<argc; i++) {
            Opml o{argv[i]};
            std::cout << o.to_json();
        }
    }
#ifdef HTML_SUPPORT
    else if (
            std::strcmp(argv[1], "-m") == 0 ||
            std::strcmp(argv[1], "--html") == 0
    ) {
        bool metadata_only = false;

        if (std::strcmp(argv[2], "--metadata-only") == 0) {
            begin = 3;
            metadata_only = true;
        }

        for (int i=begin; i<argc; i++) {
            Html h{argv[i]};
            std::cout << h.to_json(metadata_only);
        }
    }
#endif
    else usage(argv[0]);

    return 0;
}
