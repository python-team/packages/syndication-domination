# [Syndication Domination](https://gitlab.com/gabmus/syndication-domination)

An RSS/Atom parser, because there's nothing else out there.

**WARNING**: this is work in progress, not suitable for use at this point.

## Dependencies

- [pugixml](https://github.com/zeux/pugixml)
- [pybind11](https://github.com/pybind/pybind11) for the python bindings
- python3 if you intend on using or building the python bindings
- [tidy](https://github.com/htacg/tidy-html5)
- [fmt](https://github.com/fmtlib/fmt)

### Build dependencies

- [meson](https://mesonbuild.com/)

## Build options:

| Option | Type | Description | Default |
|---|---|---|---|
| `profile` | string, can either be `release` or `debug` | `release`  disables assertions, `debug` enables debug symbols | `release` |
| `python_bindings` | bool | build the python bindings | `true` |
| `to_json_binary` | bool | build the binary defined by `src/main.cpp`, see [Usage](#usage) for more info. | `false` |

## Usage

You can use the `run.sh` script to build and run the main program. Follow the usage guide provided by the program itself.

## Documentation

You can build the documentation with `doxygen Doxyfile`.

An online version tracking the `master` branch is available [on GitLab pages](https://gabmus.gitlab.io/syndication-domination).

## Using the python bindings

You'll need to build using the option `-Dpython_bindings=true` (which should be enabled by default regardless).

Then either add the install directory to your python path, or install the library in your existing python path.

The module name is `syndom`.

The python interface exposes the following classes:

- `Feed`
- `FeedItem`
- `Opml`
- `OpmlItem`
- `Html`

Of these, only `Feed`, `Opml` and `Html` can be instanced directly, this is because `FeedItem` and `OpmlItem` are instanced when creating `Feed` and `Opml` as their items.

The API is the same as described in the [documentation](https://gabmus.gitlab.io/syndication-domination) for C++, with the addition of read-only properties as an alternative to the getter methods. For example to get a `FeedItem`'s title you can either call `my_feed_item.get_title()` or simply `my_feed_item.title`.
